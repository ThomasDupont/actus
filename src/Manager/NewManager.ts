import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Toast } from '@ionic-native/toast';
import xml2js from 'xml2js';
import { EnvVariables } from "../environments/env.token";
import { NewsbaseProvider } from "../providers/newsbase/newsbase";

@Injectable()
export class NewsManager {

    private headers: HttpHeaders = new HttpHeaders();

    constructor(
        private http: HttpClient,
        @Inject(EnvVariables) public envVariable,
        private db : NewsbaseProvider,
        private toast: Toast
    ) {
        this.refresh();
    }

    public refresh(): void
    {
        const iterator = this.iterateToNotice()
        iterator.next()
        this.toast.show(`News download start`, '5000', 'center')
        this.envVariable.newsList.map(rss => {
            this.db.deleteArticles(rss.name);
            rss.rssUrl.map(url => this.getRssData(url)
                .then(r => {
                    r.rss.channel.item.forEach(async a =>
                        await this.db.createArticle(rss.name, a.title, a.description, '', a.link, new Date(a.pubDate).getTime())
                    )
                    this.notice(iterator)
                }).catch(() => this.notice(iterator))
            )
        });
    }
    /**
     *
     * @param {string} url
     * @returns {Promise<string>}
     */
    private getRssData(url: string): Promise<any>
    {
        return new Promise<any>((resolve, reject) =>
            this.http.get(url, {
                headers: this.headers.append('Accept', 'application/rss+xml'),
                responseType: 'text'
            }).subscribe(r =>
                this.convertToJson(r)
                    .then(r => resolve(r))
                    .catch(e => reject(e))
            )
        );
    }

    /**
     *
     * @param {string} data
     * @returns {Promise<string>}
     */
    private convertToJson(data: string): Promise<string>
    {
        return new Promise<string>((resolve, reject) =>
            xml2js.parseString(data, { explicitArray: false }, (error, result) => {
                return error ? reject(new Error(error).message) : resolve(result);
            })
        );
    }

    /**
     *
     * @param {IterableIterator<string>} iterator
     */
    private notice (iterator: IterableIterator<string>) : void
    {
        if (iterator.next().done) {
            alert('done')
            this.toast.show(`News download done`, '5000', 'center')
        }
    }

    /**
     *
     * @returns {IterableIterator<string>}
     */
    private *iterateToNotice() : IterableIterator<string>
    {
        for (let url of this.envVariable.newsList) {
            yield* url.rssUrl;
        }
    }
}