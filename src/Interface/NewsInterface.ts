export interface NewsInterface {
    name: string,
    rssUrl: string,
    note: string,
    icon: string
}