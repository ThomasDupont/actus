export interface ArticleInterface {
    publish: Date,
    title: string,
    picture: string,
    summary: string,
    url: string
}