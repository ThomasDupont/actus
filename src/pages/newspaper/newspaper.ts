import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NewsInterface } from "../../Interface/NewsInterface";
import { NewsbaseProvider } from "../../providers/newsbase/newsbase";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ArticleInterface } from "../../Interface/ArticleInterface";

@Component({
    selector: 'page-newspaper',
    templateUrl: 'newspaper.html',
})

export class NewspaperPage {

    selectedItem: NewsInterface;
    articleList: Array<ArticleInterface>;

    constructor(public navCtrl: NavController, public navParams: NavParams, private db : NewsbaseProvider, private iab: InAppBrowser) {
        this.selectedItem = navParams.get('item');
    }

    ionViewDidLoad() {
        this.db.getArticles(this.selectedItem.name).then(r => this.articleList = r);
    }

    /**
     *
     * @param {string} url
     */
    public openArticle(url: string): void
    {
        const browser = this.iab.create(url, '_blank', 'location=no,clearcache=yes,clearsessioncache=yes');
        browser.executeScript({code : "window.f=document.getElementsByTagName('iframe');for(var i=0;i<f.length;i++)window.f[i].remove();"})
    }

}
