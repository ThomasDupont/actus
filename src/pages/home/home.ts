import { Component, Inject } from '@angular/core';
import { NewsManager } from "../../Manager/NewManager";
import { EnvVariables } from "../../environments/env.token";
import { Network } from '@ionic-native/network';
import { Toast } from '@ionic-native/toast';
import {parseActuTest} from "../../test/parseActuTest";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {
    constructor(
        public newsManager: NewsManager,
        @Inject(EnvVariables) public envVariables,
        private network: Network,
        private toast: Toast,
        private parseActuTest: parseActuTest
    ) {
        if (this.network.type ===  'none' || this.network.type === 'cellular') {
            this.toast.show('No connection', '5000', 'center').subscribe(
                toast => {
                    console.log(toast);
                }
            );
        }
    }

    public refresh(): void
    {
        this.newsManager.refresh();
    }
}
