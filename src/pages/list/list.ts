import { Component, Inject } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NewsManager } from "../../Manager/NewManager";
import { NewsInterface } from "../../Interface/NewsInterface";
import { NewspaperPage } from "../newspaper/newspaper";
import { EnvVariables } from "../../environments/env.token";

@Component({
    selector: 'page-list',
    templateUrl: 'list.html'
})
export class ListPage {

    icons: string[];
    news: Array<NewsInterface>;

    constructor(public navCtrl: NavController, public navParams: NavParams, public newsManager: NewsManager, @Inject(EnvVariables) public envVariable) {
        this.news = envVariable.newsList;
    }

    itemTapped(event, item) {
        this.navCtrl.push(NewspaperPage, {
            item: item
        });
    }
}
