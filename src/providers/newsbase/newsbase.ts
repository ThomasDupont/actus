import { Injectable, Inject } from '@angular/core';
import { SQLite } from 'ionic-native';
import { EnvVariables } from "../../environments/env.token";
import {ArticleInterface} from "../../Interface/ArticleInterface";

@Injectable()
export class NewsbaseProvider {

    private storage: any;
    private isOpen: boolean = false;

    /**
     *
     * @param envVariable
     */
    public constructor(@Inject(EnvVariables) public envVariable) {
        if(!this.isOpen) {
            const sql = "CREATE TABLE IF NOT EXISTS articles (id INTEGER PRIMARY KEY AUTOINCREMENT, newspaper TEST, time INTEGER, title TEXT, summary TEXT, picture TEXT, url TEXT)";
            switch (this.envVariable.database) {
                case 'WebSQL':
                    if (typeof window['openDatabase'] !== "undefined") {
                        this.storage = window['openDatabase']('data', '1.0', 'database', 2000000);
                        this.storage.transaction(tx => tx.executeSql(sql, []));
                        this.storage.transaction(tx => tx.executeSql("CREATE UNIQUE INDEX IF NOT EXISTS unique_name ON articles (title)", []));
                        this.isOpen = true;
                    } else {
                        throw new Error('no webSQL driver prodided')
                    }

                    break;
                case 'SQLite':
                    this.storage = new SQLite();
                    this.storage.openDatabase({name: "data.db", location: "default"}).then(() => {
                        this.storage.executeSql(sql, []);
                        this.storage.transaction(tx => tx.executeSql("CREATE UNIQUE INDEX IF NOT EXISTS unique_name ON articles (title)", []));
                        this.isOpen = true;
                    });
                    break;
            }
        }
    }

    /**
     *
     * @returns {Promise<Array<ArticleInterface>>}
     */
    public async getArticles(newspaper: string): Promise<Array<ArticleInterface>>
    {
        const data = await this.sqlWrapper("SELECT * FROM articles WHERE newspaper = ? ORDER BY time DESC", [newspaper]);
        let articles = [];
        if(data.rows.length > 0) {
            for(let i = 0; i < data.rows.length; i++) {
                let date = new Date();
                date.setTime(data.rows.item(i).time);
                articles.push({
                    publish: date,
                    title: data.rows.item(i).title,
                    summary: data.rows.item(i).summary,
                    picture: data.rows.item(i).picture,
                    url: data.rows.item(i).url
                });
            }
        }

        return articles;
    }

    /**
     *
     * @param {string} newspaper
     */
    public deleteArticles(newspaper: string): void
    {
        this.sqlWrapper("DELETE FROM articles WHERE newspaper = ?", [newspaper]).catch(e => console.error(new Error(e).message));
    }

    /**
     *
     * @param {string} title
     * @param {string} summary
     * @param {string} picture
     * @param {string} url
     * @returns {Promise<any>}
     */
    public createArticle(newspaper: string, title: string, summary: string, picture: string, url: string, time: number): Promise<any>
    {
        return this.sqlWrapper(
            "INSERT INTO articles (newspaper, title, summary, picture, url, time) VALUES (?, ?, ?, ?, ?, ?)",
            [newspaper, title, summary, picture, url, time]
        );
    }

    /**
     *
     * @param {string} sql
     * @param {Array<any>} params
     * @returns {Promise<any>}
     */
    private sqlWrapper(sql: string, params: Array<any>) :Promise<any>
    {
        return new Promise((resolve, reject) => {
            switch (this.envVariable.database) {
                case 'WebSQL':
                    this.storage.transaction(tx => tx.executeSql(
                        sql,
                        params,
                        (t, data) => resolve(data),
                        (t, error) => reject(error)
                    ));
                    break;
                case 'SQLite':
                    this.storage.executeSql(sql, params).then(
                        data => resolve(data),
                        error => reject(error)
                    );
                    break;
            }
        });
    }

}
