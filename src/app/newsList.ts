export const newsList = [
  {
    name: 'le-monde',
    rssUrl: [
      'https://www.lemonde.fr/m-actu/rss_full.xml',
      'https://www.lemonde.fr/ameriques/rss_full.xml',
      'https://www.lemonde.fr/argent/rss_full.xml',
      'https://www.lemonde.fr/asie-pacifique/rss_full.xml',
      'https://www.lemonde.fr/les-decodeurs/rss_full.xml',
      'https://www.lemonde.fr/sciences/rss_full.xml',
      'https://www.lemonde.fr/economie/rss_full.xml'
    ],
    note: '',
    icon: 'flask'
  },
  {
    name: 'le-figaro',
    rssUrl: [
        'http://www.lefigaro.fr/rss/figaro_actualites.xml',
        'http://www.lefigaro.fr/rss/figaro_flash-actu.xml',
        'http://www.lefigaro.fr/rss/figaro_international.xml',
        'http://www.lefigaro.fr/rss/figaro_actualite-france.xml',
        'http://www.lefigaro.fr/rss/figaro_sciences.xml',
        'http://www.lefigaro.fr/rss/figaro_vox.xml',
        'http://www.lefigaro.fr/rss/figaro_lefigaromagazine.xml'
    ],
    note: '',
    icon: 'cloudy-night'
  },
  {
    name: 'les-echos',
    rssUrl: [
        'https://www.lesechos.fr/rss/rss_une_titres.xml',
        'https://syndication.lesechos.fr/rss/rss_politique_societe.xml',
        'https://syndication.lesechos.fr/rss/rss_france.xml',
        'https://syndication.lesechos.fr/rss/Echos_monde.xml',
        'https://syndication.lesechos.fr/rss/rss_tech_medias.xml',
        'https://syndication.lesechos.fr/rss/rss_industries_services.xml',
        'https://syndication.lesechos.fr/rss/rss_finance-marches.xml',
        'https://syndication.lesechos.fr/rss/rss_pme-innovation.xml',
        'https://syndication.lesechos.fr/rss/rss_idee.xml',
        'https://syndication.lesechos.fr/rss/rss_echos-patrimoine.xml',
        'https://syndication.lesechos.fr/rss/rss_automobile.xml',
        'https://syndication.lesechos.fr/rss/rss_transports.xml',
        'https://syndication.lesechos.fr/rss/rss_aero.xml',
        'https://syndication.lesechos.fr/rss/rss_conso.xml',
        'https://syndication.lesechos.fr/rss/rss_mode_luxe.xml',
        'https://syndication.lesechos.fr/rss/rss_energie-environnement.xml',
        'https://syndication.lesechos.fr/rss/rss_sante.xml',
        'https://syndication.lesechos.fr/rss/rss_service.xml',
        'https://syndication.lesechos.fr/rss/rss_immobilier_btp.xml',
        'https://syndication.lesechos.fr/rss/rss_industrie.xml'
    ],
    note: '',
    icon: 'analytics'
  },
  {
    name: 'developpez',
    rssUrl: ['https://web.developpez.com/index/rss'],
    note: '',
    icon: 'git-branch'
  },
  {
    name: 'challenge',
    rssUrl: ['https://www.challenges.fr/rss.xml'],
    note: '',
    icon: 'podium'
  },
  {
    name: 'liberation',
    rssUrl: ['http://rss.liberation.fr/rss/latest/'],
    note: '',
    icon: 'wifi'
  },
  {
    name: 'nouvel-obs',
    rssUrl: [
        'http://www.nouvelobs.com/rss.xml',
        'http://www.nouvelobs.com/politique/rss.xml',
        'http://www.nouvelobs.com/societe/rss.xml',
        'http://www.nouvelobs.com/monde/rss.xml',
        'http://www.nouvelobs.com/economie/rss.xml',
        'http://www.nouvelobs.com/culture/rss.xml',
        'http://www.nouvelobs.com/education/rss.xml',
        'http://www.nouvelobs.com/rue89/rss.xml',
        'http://www.nouvelobs.com/rue89/rue89-nos-vies-connectees/rss.xml',
    ],
    note: '',
    icon: 'beaker'
  },
  {
    name: 'marianne',
    rssUrl: ['https://www.marianne.net/rss.xml'],
    note: '',
    icon: 'locate'
  },
  {
    name: 'la-tribune',
    rssUrl: ['https://www.latribune.fr/rss/rubriques/actualite.html'],
    note: '',
    icon: 'nuclear'
  },
  {
    name: 'atlantico',
    rssUrl: ['http://www.atlantico.fr/rss.xml'],
    note: '',
    icon: 'planet'
  }
]