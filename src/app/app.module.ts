import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NewsManager } from "../Manager/NewManager";
import { NewspaperPage } from "../pages/newspaper/newspaper";
import { NewsbaseProvider } from '../providers/newsbase/newsbase';
import { EnvironmentsModule } from "../environments/environment-variables.module.";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { Network } from "@ionic-native/network";
import { Toast } from "@ionic-native/toast";
import {parseActuTest} from "../test/parseActuTest";

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        NewspaperPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        HttpClientModule,
        EnvironmentsModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        NewspaperPage
    ],
    providers: [
        StatusBar,
        NewsManager,
        parseActuTest,
        SplashScreen,
        NewsbaseProvider,
        InAppBrowser,
        Network,
        Toast,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {}
