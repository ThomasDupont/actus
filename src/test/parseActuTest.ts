import { HttpClient, HttpHeaders } from "@angular/common/http";
import xml2js from 'xml2js';
import { Injectable, Inject } from "@angular/core";
import {NewsbaseProvider} from "../providers/newsbase/newsbase";
import {EnvVariables} from "../environments/env.token";

@Injectable()
export class parseActuTest {

    private headers: HttpHeaders = new HttpHeaders();

    constructor(
        private http: HttpClient,
        private db: NewsbaseProvider,
        @Inject(EnvVariables) private envVariable
    ) {
        if (this.envVariable.env === 'development') {
            [
                'assets/lemonde.xml',
                'assets/lemonde-amerique.xml'
            ].map(e => this.getArticle(e));
        }
    }

    private getArticle(url: string): void
    {
        this.http.get(url, {
            headers: this.headers.append('Accept', 'application/rss+xml'),
            responseType: 'text'
        }).subscribe(r => this.convertToJson(r).then(r => {
            this.db.deleteArticles('le-monde');
            r.rss.channel.item.map(e => {
                this.db.createArticle('le-monde', e.title, e.description, '', e.link, new Date(e.pubDate).getTime()).catch(err => console.log(err));
            });
        }));
    }

    /**
     *
     * @param {string} data
     * @returns {Promise<string>}
     */
    private convertToJson(data: string): Promise<any>
    {
        return new Promise<any>(resolve =>
            xml2js.parseString(data, { explicitArray: false }, (error, result) => {
                if (error) {
                    throw new Error(error);
                } else {
                    resolve(result);
                }
            })
        );
    }
}