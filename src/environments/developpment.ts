import { newsList } from "../app/newsList";

export const CONF_DEV = {
    newsList,
    database: 'WebSQL',
    env: 'development'
};