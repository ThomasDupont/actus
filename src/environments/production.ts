import { newsList } from "../app/newsList";

export const CONF_PROD = {
    newsList,
    database: 'SQLite',
    env: 'production'
}