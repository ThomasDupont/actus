import { NgModule } from '@angular/core';
import { EnvVariables } from './env.token';
import { CONF_DEV } from './developpment';
import { CONF_PROD } from './production';

declare const process: any; // Typescript compiler will complain without this
export function environmentFactory() {
    return process.env.IONIC_ENV === 'dev' ? CONF_DEV : CONF_PROD;
}

@NgModule({
    providers: [
        {
            provide: EnvVariables,
            useFactory: environmentFactory
        }
    ]
})
export class EnvironmentsModule {}